//Crie uma função que receba um número como parâmetro e retorne "par" se o número for par e "ímpar" se o número for ímpar.
namespace exercicio4{
    function numero(n:number){
        if(n % 2 !== 0){
            console.log("O numero é impar");
        }
        else{
            console.log("O numero é par");
            
        }
    }
    numero(32);
}