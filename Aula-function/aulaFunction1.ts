namespace aulaFunction1 {
    function saudacao(nome?:string){
        if (nome){
            console.log(`Ola, ${nome}`);
        } else{
            console.log("Ola, estranho!");
        }
    }

    saudacao('vinicius');

    function potencia(base:number, expoente:number = 2  ){
        console.log(Math.pow(base, expoente));
    }

    potencia(3);
    potencia(3, 3)
}