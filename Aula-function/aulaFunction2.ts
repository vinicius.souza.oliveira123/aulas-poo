namespace aulaFunction2{
    function multiplicaPor(n:number) : (x:number) => number {
        return function (x:number):number {
            return x * n;
        }
    }
    let duplicar = multiplicaPor(2);
    let triplicar = multiplicaPor(3);
    let resultado = duplicar(5);

    console.log(resultado);
    console.log(triplicar(5));
    
    

}