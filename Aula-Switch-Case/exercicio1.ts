//Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido:

namespace exercicio1 
{
    let NivelDoUsuario: number
    NivelDoUsuario = 2

    switch (NivelDoUsuario)
    {
        case 1:console.log("Seu nivel é basico");
            break;
        case 2:console.log("Seu nivel é intermediario");
            break;
        case 3:console.log("Seu nivel é avançado"); 
            break;
        default:console.log("Numero não reconhecido");
    }

}