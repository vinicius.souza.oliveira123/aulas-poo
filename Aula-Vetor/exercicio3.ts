//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.
namespace exercicio3{
    let obj: any = {
        nome: "Vinicius",
        idade: 16,
        email: "vinicius.souza.oliviera123@gmail.com"

    }
    console.log(obj);
    let livros: any[] = [
        {titulo: "Titulo 1", autor: "Autor 1"},
        {titulo: "Titulo 2", autor: "Autor 2"},
        {titulo: "Titulo 3", autor: "Autor 3"},
        {titulo: "Titulo 4", autor: "Autor 4"},
        {titulo: "Titulo 5", autor: "Autor 5"},

    ];
    let titulos = livros.map((livro) => {
        return livro.titulo
    });
    console.log(titulos);
    

}