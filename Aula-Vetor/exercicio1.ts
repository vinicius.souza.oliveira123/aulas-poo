//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.

namespace exercicio1 
{
    let num: number[] = [10, 20, 30 , 40, 50];
    let qtdNum: number = num.length;
    let somaDosNum: number = 0 ;

    for (let i = 0; i < qtdNum; i++){
        somaDosNum += num[i];
    }

    console.log(`A soma dos numeros são: ${somaDosNum}`);

}