//Crie um vetor chamado "alunos contendo três objetos, cada um representando um aluno com as seguintes propriedades: "nome" (string), "idade" (number) e "notas" (array de números).Preencha o vetor com informações ficticias. 
 //Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente com o nome e a idade do aluno.

interface Aluno{
    nome: string;
    idade: number;
    notas: number[];
}
namespace exercicioBonus2{
    const alunos: Aluno[] = [
        {nome: "Aluno1", idade: 20, notas:[4,7,8]},
        {nome: "Aluno2", idade: 23, notas:[2,5,10]},
        {nome: "Aluno3", idade: 49, notas:[10,9,1]},
        {nome: "Aluno4", idade: 18, notas:[3,4,2]},
        {nome: "Aluno5", idade: 33, notas:[1,10,5]}
    ]

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce (
            (total, notas) => {return total + notas}
        ) / aluno.notas.length;

        if (media >= 7)
        {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
        }
        else
        {
            console.log(`A média do aluno: ${aluno.nome} é igual a ${media} e o aluno está reprovado`);
        
        }
    })
}