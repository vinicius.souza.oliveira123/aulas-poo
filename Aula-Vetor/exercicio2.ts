//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.
namespace exercicio2
{
    let frutas: string[] = ["Maça", "Morango", "Banana"];
    let qtdFrutas: number = frutas.length
    let i = 0

    while(i < qtdFrutas){
        console.log(` ${frutas[i]}  `);
        i ++
    }
}