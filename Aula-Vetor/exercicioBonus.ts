 //Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que ultilize a função filter() para encontrar todos os livros do autor com valor "Autor3". Em seguida, utilize a função map() para mostrar apenas os titulos dos livros encontrados. O resultado deve ser exibido no console.

namespace exercicioBonus {
    let livro: any[] = [
        {titulo:"titulo1", autor: "autor3"},
        {titulo:"titulo2", autor: "autor3"},
        {titulo:"titulo3", autor: "autor3"},
        {titulo:"titulo4", autor: "autor3"},
        {titulo:"titulo5", autor: "autor2"},
        {titulo:"titulo6", autor: "autor4"},
        {titulo:"titulo7", autor: "autor4"},
        {titulo:"titulo8", autor: "autor2"},
        {titulo:"titulo9", autor: "autor1"},
    ]
    let resultados = livro.filter((livro) => {
        return livro.autor === "autor3"
    }
    );

    console.log(resultados);

    let titulos = resultados.map((resultados) => {
        return resultados.titulo
    });
    console.log(titulos);
}