// Escreva um programa TypeScript que imprima todos os números primos de 1 a 53 usando a função while.
namespace exercicio1
{
    let numero, aux: number;
    let primo: boolean;
    
    numero = 0;
    
    while (numero <=53)
    {
        aux=2;
        primo = true;
        while(aux < numero)
        {
            if(numero % aux == 0)
            {
                primo = false;
                break;
            }
        aux++;
        }
        if (primo && numero != 1)
        {
            console.log(`${numero}`);
            
        }
        numero++;
    }

}
